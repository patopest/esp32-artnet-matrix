Know Issues
===========

# Current

## Netif
- IP event handler does not unregister instances so succesive calls to set_ip_type() will trigger multiple calls to the handler... Is it my code or a bug in esp-idf with the event relaying from LWip to Event to App: [wifi-driver docs](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/wifi.html)
- Need to handle wifi disconnects (keep event handlers?)

## ArtPoll
- Not checking if the priority code is correct and defined as in Table 5 of the Art-Net Specs.
Making the assumption that the controller will send a correct code.

## ArtIpProg
- Protocol specs don't specify behavior clearly and the only implementation which supports it seems to be [rpidmx](https://github.com/vanvught/rpidmx512).
- When programming port, IpProgReply is sent from old port, the socket rebinding is done after sending response.
- New network configs are passed by the node state struct. Not ideal since it's supposed to be a state and not a config struct. (i.e: if the net reconfiguration fails...)
- State changes (to send ArtPollReply packet) are done by comparing packet values to node state values. This will mark the state as changed if you send a RESET_PARAMS bit but not populate the rest of the packet.

## ArtAddress
- Not checking if state changed and rewriting the whole configs. Could maybe be optimised to only trigger a write to memory of the configs if there was a change + optimise write by only writing what was changed.

## Node
- Because this node is not running on a Class A network, the default IP generation from the Art-Net specs is not performed. Instead the default params is to have DHCP enabled. Would probably need to still perform this IP generation in case the node is first booted on a network without DHCP (need to perform fallback to static IP if that is the case).
- Not all state configs are stored in NVS. Need to double-check what would be needed.
- OEM and ESTA codes are copied from libarnet, would need to figure out/change them accordingly.
=> ESTA code is set to experimental use code 0x7FF0

## ArtDiagData
- Only supporting one message for now. Not handling different priority levels.
(could it be possible to pass some of the logs in there? would that be usable?)

## Leds (WS2812b)
- Current RMT driver creates random flickers. Based on forum post, the RMT hardware/idf drivers in ESP-32 chips are not as good as the I2S ones. Would need to rewrite that.

# Resolved

## ArtPoll
- [libartnet](https://github.com/OpenLightingProject/libartnet) is using depreciated bits. The [website](https://art-net.org.uk/how-it-works/discovery-packets/artpollreply/) states that ArtPollReplies should always be Unicasted.
- Refactor ArtPoll receipt based on the [website's](https://art-net.org.uk/how-it-works/discovery-packets/artpoll/) information.

## ArtIpProg
- When programming IP, the IpProgReply will be sent from the new IP and not the old one.
=> not ideal since we want to know that the node responding is the one we sent IpProg to and in the reply packet's data we would get the new IP to send to. BUT in the case where we set DHCP, we have to wait for the DHCP request to complete in order to obtain the new node IP to send in IpProgReply. [rpidmx](https://github.com/vanvught/rpidmx512/blob/master/lib-artnet/src/artnetnodehandleipprog.cpp) seems to be changing network configs before sending IpProgReply
=> There might be a way of doing that by creating a new node net-if interface to perform DHCP, send the reply with old one then change the active interface to the new one.
=> OR maybe but just maybe, the protocol meant for IpProgReply to just be an acknowledgement and the network changes to take place afterwards then PollReply sent after config change..... (seems likely)

