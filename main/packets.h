#ifndef ARTNET_PACKETS_H
#define ARTNET_PACKETS_H

#include <sys/types.h>
#include <stdint.h>
#include "lwip/sockets.h"

// #include "common.h"

#define PACKED __attribute__((packed))

enum { ARTNET_SHORT_NAME_LENGTH = 18 };
enum { ARTNET_LONG_NAME_LENGTH = 64 };
enum { ARTNET_REPORT_LENGTH = 64 };
enum { ARTNET_MAX_PORTS = 4 };
enum { ARTNET_DMX_LENGTH = 512};
enum { ARTNET_MSG_LENGTH = 512};
enum { ARTNET_MAC_SIZE = 6 };



/* ---------- Opcodes ---------- */
enum artnet_packet_type_e {
	ARTNET_POLL = 0x2000,
	ARTNET_REPLY = 0x2100,
	ARTNET_DIAGDATA = 0x2300,
	ARTNET_COMMAND = 0x2400,
	ARTNET_DMX = 0x5000,
	ARTNET_NZS = 0x5100,
	ARTNET_SYNC = 0x5200,
	ARTNET_ADDRESS = 0x6000,
	ARTNET_INPUT = 0x7000,
	ARTNET_TODREQUEST = 0x8000,
	ARTNET_TODDATA = 0x8100,
	ARTNET_TODCONTROL = 0x8200,
	ARTNET_RDM = 0x8300,
	ARTNET_RDMSUB = 0x8400,
	ARTNET_VIDEOSTEUP = 0xa010,
	ARTNET_VIDEOPALETTE = 0xa020,
	ARTNET_VIDEODATA = 0xa040,
	ARTNET_MACMASTER = 0xf000,
	ARTNET_MACSLAVE = 0xf100,
	ARTNET_FIRMWAREMASTER = 0xf200,
	ARTNET_FIRMWAREREPLY = 0xf300,
	ARTNET_FILETNMASTER = 0xf400,
	ARTNET_FILEFNMASTER = 0xf500,
	ARTNET_FILEFNREPLY = 0xf600,
	ARTNET_IPPROG = 0xf800,
	ARTNET_IPREPLY = 0xf900,
	ARTNET_MEDIA = 0x9000,
	ARTNET_MEDIAPATCH = 0x9100,
	ARTNET_MEDIACONTROL = 0x9200,
	ARTNET_MEDIACONTROLREPLY = 0x9300,
	ARTNET_TIMECODE = 0x9700,
	ARTNET_TIMESYNC = 0x9800,
	ARTNET_TRIGGER = 0x9900,
	ARTNET_DIRECTORY = 0x9a00,
	ARTNET_DIRECTORYREPLY = 0x9b00,
} PACKED;

typedef enum artnet_packet_type_e artnet_packet_type_t;


/* --------- Packet types --------- */
// ArtDmx
struct artnet_dmx_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  verH;
  uint8_t  ver;
  uint8_t  sequence;
  uint8_t  physical;
  uint16_t universe;
  uint8_t  lengthHi;
  uint8_t  length;
  uint8_t  data[ARTNET_DMX_LENGTH];
} PACKED;

typedef struct artnet_dmx_s artnet_dmx_t;


// ArtPoll
struct	artnet_poll_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  verH;
  uint8_t  ver;
  uint8_t  ttm;
  uint8_t  priority;
} PACKED;

typedef struct artnet_poll_s artnet_poll_t;

// ArtPollReply
struct artnet_reply_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  ip[4];
  uint16_t port;
  uint8_t  verH;
  uint8_t  ver;
  uint8_t  net;
  uint8_t  subnet;
  uint8_t  oemH;
  uint8_t  oem;
  uint8_t  ubea;
  uint8_t  status1;
  uint8_t  estaman[2];
  uint8_t  shortname[ARTNET_SHORT_NAME_LENGTH];
  uint8_t  longname[ARTNET_LONG_NAME_LENGTH];
  uint8_t  nodereport[ARTNET_REPORT_LENGTH];
  uint8_t  numbportsH;
  uint8_t  numbports;
  uint8_t  porttypes[ARTNET_MAX_PORTS];
  uint8_t  goodinput[ARTNET_MAX_PORTS];
  uint8_t  goodoutputA[ARTNET_MAX_PORTS];
  uint8_t  swin[ARTNET_MAX_PORTS];
  uint8_t  swout[ARTNET_MAX_PORTS];
  uint8_t  swvideo;
  uint8_t  swmacro;
  uint8_t  swremote;
  uint8_t  sp1;
  uint8_t  sp2;
  uint8_t  sp3;
  uint8_t  style;
  uint8_t  mac[ARTNET_MAC_SIZE];
  uint8_t  bindIp[4];
  uint8_t  bindIndex;
  uint8_t  status2;
  uint8_t  goodoutputB[ARTNET_MAX_PORTS];
  uint8_t  status3;
  uint8_t  filler[21];
} PACKED;

typedef struct artnet_reply_s artnet_reply_t;


// ArtIpProg
struct artnet_ipprog_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  proVerH;
  uint8_t  proVer;
  uint8_t  filler1;
  uint8_t  filler2;
  uint8_t  command;
  uint8_t  filler4;
  uint8_t  progIp[4];
  uint8_t  progSm[4];
  uint16_t progPort;
  uint8_t  spare1;
  uint8_t  spare2;
  uint8_t  spare3;
  uint8_t  spare4;
  uint8_t  spare5;
  uint8_t  spare6;
  uint8_t  spare7;
  uint8_t  spare8;

} PACKED;

typedef struct artnet_ipprog_s artnet_ipprog_t;

// ArtIpProgReply
struct artnet_ipprog_reply_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  proVerH;
  uint8_t  proVer;
  uint8_t  filler1;
  uint8_t  filler2;
  uint8_t  filler3;
  uint8_t  filler4;
  uint8_t  progIp[4];
  uint8_t  progSm[4];
  uint16_t progPort;
  uint8_t  status;
  uint8_t  spare2;
  uint8_t  spare3;
  uint8_t  spare4;
  uint8_t  spare5;
  uint8_t  spare6;
  uint8_t  spare7;
  uint8_t  spare8;
} PACKED;

typedef struct artnet_ipprog_reply_s artnet_ipprog_reply_t;


// ArtAddress
struct artnet_address_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  proVerH;
  uint8_t  proVer;
  uint8_t  net;
  uint8_t  bindIndex;
  uint8_t  shortname[ARTNET_SHORT_NAME_LENGTH];
  uint8_t  longname[ARTNET_LONG_NAME_LENGTH];
  uint8_t  swin[ARTNET_MAX_PORTS];
  uint8_t  swout[ARTNET_MAX_PORTS];
  uint8_t  subnet;
  uint8_t  swvideo;
  uint8_t  command;
} PACKED;

typedef struct artnet_address_s artnet_address_t;


// ArtDiagData
struct artnet_diagdata_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  proVerH;
  uint8_t  proVer;
  uint8_t  filler1;
  uint8_t  priority;
  uint8_t  filler2;
  uint8_t  filler3;
  uint16_t length;
  uint8_t  data[ARTNET_MSG_LENGTH];
} PACKED;

typedef struct artnet_diagdata_s artnet_diagdata_t;


// ArtCommand
struct artnet_command_s {
  uint8_t  id[8];
  uint16_t opCode;
  uint8_t  verH;
  uint8_t  ver;
  uint8_t  estaman[2];
  uint16_t length;
  uint8_t  data[ARTNET_MSG_LENGTH];
} PACKED;

typedef struct artnet_command_s artnet_command_t;


/* ---------- Packets ----------- */
// union of all artnet packets
typedef union {
  artnet_poll_t ap;
  artnet_reply_t ar;
  artnet_ipprog_t aip;
  artnet_ipprog_reply_t aipr;
  artnet_address_t addr;
  artnet_diagdata_t adiag;
  // artnet_timecode_t atimecode;
  artnet_command_t acmd;
  // artnet_trigger_t atrig;
  artnet_dmx_t admx;
  // artnet_sync_t async;
  // artnet_nzs_t anzs;
  // artnet_input_t ainput;
  // artnet_todrequest_t todreq;
  // artnet_toddata_t toddata;
  // artnet_todcontrol_t todcontrol;
  // artnet_firmware_t firmware;
  // artnet_firmware_reply_t firmwarer;
  // artnet_rdm_t rdm;
  // artnet_rdmsub_t rdmsub;
} artnet_packet_union_t;


// a packet, containing data, length, type, a src/dst address and src/dest port
typedef struct {
  int length;
  struct in_addr from;
  struct in_addr to;
  uint16_t port;
  artnet_packet_type_t type;
  artnet_packet_union_t data;
} artnet_packet_t;

typedef artnet_packet_t *artnet_packet;




#undef PACKED

#endif

