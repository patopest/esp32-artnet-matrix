#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdbool.h>
#include "lwip/sockets.h"

#include "packets.h"

#define FM_VERSION_MAJOR  (0)
#define FM_VERSION_MINOR  (1)

/*
 * bit-field for the port address
 */
// struct port_address_s {
//  uint8_t bit_15 : 1;
//  uint8_t net : 7;
//  uint8_t subnet : 4;
//  uint8_t universe : 4;
// };

// typedef struct port_address_s port_address_t;

// ArtPoll TalktoMe masks
typedef enum {
  TTM_ONCHANGE_SEND = 0x02,  /**< Send ArtPollReply on status change >**/
  TTM_DIAG_TX = 0x04,        /**< Send diagnostic messages >**/
  TTM_DIAG_TYPE = 0x08,      /**< Broadcast/Unicast diagnostic messages >**/
  TTM_VLC = 0x10             /**< Enable VLC transmission >**/
} ttm_bit_mask;

// ArtIpProg command masks
typedef enum {
  IPPROG_PROGRAM = 0x80,    /**< Program >**/
  IPPROG_DHCP = 0x40,       /**< Enable DHCP >**/
  IPPROG_DEFAULT = 0x08,    /**< Reset to default >**/
  IPPROG_IP = 0x04,         /**< Program IP >**/
  IPPROG_NETMASK = 0x02,    /**< Program Netmask >**/
  IPPROG_PORT = 0x01        /**< Program port >**/
} ipprog_bit_mask;


// node report codes
typedef enum {
  ARTNET_RCDEBUG,
  ARTNET_RCPOWEROK,
  ARTNET_RCPOWERFAIL,
  ARTNET_RCSOCKETWR1,
  ARTNET_RCPARSEFAIL,
  ARTNET_RCUDPFAIL,
  ARTNET_RCSHNAMEOK,
  ARTNET_RCLONAMEOK,
  ARTNET_RCDMXERROR,
  ARTNET_RCDMXUDPFULL,
  ARTNET_RCDMXRXFULL,
  ARTNET_RCSWITCHERR,
  ARTNET_RCCONFIGERR,
  ARTNET_RCDMXSHORT,
  ARTNET_RCFIRMWAREFAIL,
  ARTNET_RCUSERFAIL
} artnet_node_report_code;

// node style
typedef enum {
  ST_NODE = 0x00,         /**< An DMX to/from Art-Net device (node) */
  ST_CONTROLLER = 0x01,   /**< A lighting console */
  ST_MEDIA = 0x02,        /**< A Media Server */
  ST_ROUTE = 0x03,        /**< A network routing device*/
  ST_BACKUP = 0x04,       /**< A backup device */
  ST_CONFIG = 0x05,       /**< A configuration or diagnostic tool */
  ST_VISUAL = 0x06        /**< A visualiser */  
} artnet_node_style_code;

// ArtDiag message priority
typedef enum {
  DP_LOW = 0x10,       /**< low priority message */
  DP_MED = 0x40,       /**< medium priority message */
  DP_HIGH = 0x80,      /**< high priority message */
  DP_CRITICAL = 0xe0,  /**< critical priority message */
  DP_VOLATILE = 0xf0   /**< volatile message */
} artnet_priority_code;


// enum { ARTNET_SHORT_NAME_LENGTH = 18 };
// enum { ARTNET_LONG_NAME_LENGTH = 64 };
// enum { ARTNET_REPORT_LENGTH = 64 };
// enum { ARTNET_MAX_PORTS = 4 };
// enum { ARTNET_DMX_LENGTH = 512};
// enum { ARTNET_MSG_LENGTH = 512};
// enum { ARTNET_MAC_SIZE = 6 };


// the status of the node
typedef enum {
  ARTNET_OFF,
  ARTNET_STANDBY,
  ARTNET_ON
} node_status_t;

// struct to hold the state of the node
typedef struct {
  // artnet_node_type node_type;
  node_status_t status;
  struct in_addr ip_addr;
  struct in_addr net_mask;
  struct in_addr reply_addr;
  struct in_addr bcast_addr;
  bool dhcp;
  uint16_t port;
  uint8_t mac_addr[ARTNET_MAC_SIZE];
  
  bool send_diag;
  bool diag_unicast;
  uint8_t diag_priority;
  bool send_apr_on_change;
  uint32_t reply_count;

  char short_name[ARTNET_SHORT_NAME_LENGTH];
  char long_name[ARTNET_LONG_NAME_LENGTH];
  char report[ARTNET_REPORT_LENGTH];
  uint8_t net;
  uint8_t subnet;
  uint8_t oem_hi;
  uint8_t oem_lo;
  uint8_t esta_hi;
  uint8_t esta_lo;
  // int bcast_limit; // the number of nodes after which we change to bcast
  artnet_node_report_code report_code;
} node_state_t;


// struct holding the system state (not artnet related)
typedef struct {
  uint32_t restart_counter;
} system_state_t;


/**
 * The main node structure
 */
typedef struct artnet_node_s{
  // artnet_socket_t sd;      // the two sockets
  artnet_node_style_code style;
  node_state_t state;         // the state struct
  system_state_t sys_state;   // the system state
  // node_callbacks_t callbacks;  // the callbacks struct
  // struct ports_s {
  //   uint8_t  types[ARTNET_MAX_PORTS];    // type of port
  //   input_port_t in[ARTNET_MAX_PORTS];   // input ports
  //   output_port_t out[ARTNET_MAX_PORTS]; // output ports
  // } ports;
  artnet_reply_t ar_temp;       // buffered artpoll reply packet
  // node_list_t node_list;        // node list
  // firmware_transfer_t firmware; // firmware details
  // node_peering_t peering;       // peer if we've joined a group
} artnet_node_t;

typedef artnet_node_t *artnet_node;

/** The local ArtNet node */
extern artnet_node node;


// defined in ArtNet.c
extern uint32_t ARTNET_PORT;
extern uint8_t ARTNET_VERSION;

extern void set_node_default_network_config(artnet_node n);




#endif