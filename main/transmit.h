#ifndef TRANSMIT_H
#define TRANSMIT_H

#include <stdbool.h>

#include "common.h"
#include "packets.h"


int artnet_tx_poll_reply(artnet_node n, artnet_packet poll, bool response);
int artnet_tx_ipprog_reply(artnet_node n, artnet_packet p);
int artnet_tx_diagdata(artnet_node n, artnet_priority_code priority);

int artnet_tx_build_art_poll_reply(artnet_node n);

#endif