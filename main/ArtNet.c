#define LOG_LOCAL_LEVEL     ESP_LOG_DEBUG

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include "wifi.h"
#include "udp_server.h"
#include "common.h"
#include "transmit.h"
#include "memory.h"
#include "led.h"


/* --------- Global Variables --------- */
uint32_t ARTNET_PORT = 6454;
uint8_t ARTNET_VERSION = 14;


/* --------- Local Variables --------- */
static const char *TAG = "artnet";

uint8_t OEM_HI = 0x04;
uint8_t OEM_LO = 0x30;
char ESTA_HI = 0x7F;    // ESTA code from https://tsp.esta.org/tsp/working_groups/CP/mfctrIDs.php
char ESTA_LO = 0xF0;    // 0x7FF0 is for dev/experimental use
char SHORT_NAME[] = "ESP32-ArtNet";
char LONG_NAME[] = "ESP32-ArtNet-Node";

artnet_priority_code PRIORITY_CODE = DP_VOLATILE; // basically do not send diag data for now

// The local ArtNet node
artnet_node node;
// The LED matrix
led_strip led_matrix;


/* --------- Local Functions --------- */
static void init_artnet_node();
static void start_artnet_node();
static void init_logging();



static void init_artnet_node() {

    node = (artnet_node) malloc(sizeof(artnet_node_t));

    memset(node, 0x0, sizeof(artnet_node_t));

    node->style = ST_NODE;
    node->sys_state.restart_counter = get_restart_counter();

    node->state.send_apr_on_change = false;
    node->state.reply_count = 0;
    node->state.send_diag = true;
    node->state.diag_unicast = 0;
    node->state.diag_priority = DP_MED;
    
    set_node_default_network_config(node);
    node->state.reply_addr.s_addr = 0;

    memcpy(&node->state.short_name, &SHORT_NAME, sizeof(SHORT_NAME));
    memcpy(&node->state.long_name, &LONG_NAME, sizeof(LONG_NAME));
    node->state.oem_hi = OEM_HI;
    node->state.oem_lo = OEM_LO;
    node->state.esta_hi = ESTA_HI;
    node->state.esta_lo = ESTA_LO;

    node->state.net = 0;
    node->state.subnet = 0;

    load_node_configs(node);

    node->state.status = ARTNET_STANDBY;
    node->state.report_code = ARTNET_RCPOWEROK;

}

void set_node_default_network_config(artnet_node n) {

    esp_read_mac((uint8_t *)&n->state.mac_addr, ESP_MAC_WIFI_STA); // get WiFi MAC address
    n->state.dhcp = true;
    n->state.port = ARTNET_PORT;

    // technically Art-Net specs define how to generate IP from mac on boot if not configured
    // but we're not doing that because it's not running on class A networks
}

 
static void start_artnet_node() {

    node->state.status = ARTNET_ON;

    if (node->state.reply_addr.s_addr == 0) {
        node->state.reply_addr = node->state.bcast_addr;
    }

    save_node_configs(node);

    // build and send PollReply on startup
    artnet_tx_build_art_poll_reply(node);
    artnet_tx_poll_reply(node, NULL, false);

    // Send Diag message on startup
    artnet_tx_diagdata(node, DP_MED);
    node->state.diag_priority = DP_HIGH;
}



static void init_logging() {
    // ESP-IDF modules
    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("wifi", ESP_LOG_INFO);

    // local modules
    esp_log_level_set("memory", ESP_LOG_INFO);
    esp_log_level_set("wifi station", ESP_LOG_INFO);
    esp_log_level_set("udp server", ESP_LOG_INFO);
    esp_log_level_set("artnet receive", ESP_LOG_DEBUG);
    esp_log_level_set("artnet transmit", ESP_LOG_INFO);
    esp_log_level_set("led", ESP_LOG_INFO);

}

void app_main(void)
{
    init_logging();
    init_memory();
    set_restart_counter();
    init_artnet_node();
    init_wifi_sta();
    // init_ledc();
    led_matrix = init_led_strip();

    ESP_LOGI(TAG, "Hello");

    xTaskCreate(led_strip_task, "led_matrix", 4096, led_matrix, 4, NULL);
    xTaskCreate(udp_server_task, "udp_server", 4096, (void*)AF_INET, 5, NULL);

    start_artnet_node();    // TO CHANGE: need to server task to be started to send but it could be scheduled differently

}
