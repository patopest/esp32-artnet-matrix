#define LOG_LOCAL_LEVEL     ESP_LOG_DEBUG

#include <string.h>
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"


#include "memory.h"
#include "common.h"


/* --------- Local Variables --------- */
static const char *TAG = "memory";

static nvs_handle_t system_handle;
static nvs_handle_t artnet_handle;

static uint32_t restart_counter;


/* --------- Local Functions --------- */
static void init_handle(char *namespace, nvs_handle_t *handle);

static void mem_get_u8(nvs_handle_t handle, char *key, void *to);
static void mem_get_u16(nvs_handle_t handle, char *key, void *to);
static void mem_get_u32(nvs_handle_t handle, char *key, void *to);
static void mem_get_str(nvs_handle_t handle, char *key, void *to, size_t length);



// init stuff
void init_memory() {
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    init_handle("system", &system_handle);
    init_handle("artnet", &artnet_handle);

}

static void init_handle(char *namespace, nvs_handle_t *handle) {

    esp_err_t err = nvs_open(namespace, NVS_READWRITE, handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error opening NVS handle for %s: 0x%x", namespace, err);
    }
    else {
        ESP_LOGD(TAG, "Successfully init NVS handle for %s", namespace);
    }
}


// Restart counter
void set_restart_counter() {

    esp_err_t err;
    restart_counter = 0;

    mem_get_u32(system_handle, "restart_counter", &restart_counter);

    ESP_LOGI(TAG, "Restart counter: %d", restart_counter);

    restart_counter++;

    ESP_LOGI(TAG, "Updating restart counter in NVS ... ");
    err = nvs_set_u32(system_handle, "restart_counter", restart_counter);
    ESP_ERROR_CHECK(err);
    err = nvs_commit(system_handle);
    ESP_ERROR_CHECK(err);

}

uint32_t get_restart_counter() {
    return restart_counter;
}

// artnet node configs
void load_node_configs(artnet_node n) {

    /* list of configs we want to store in NVS
    - ip
    - net mask
    - broadcast address (nope, will be recomputed)
    - dhcp status
    - port

    - short name
    - long name

    - net
    - subnet
    - universes (of ports)
    */

    ESP_LOGI(TAG, "Loading node configs to NVS");

    // network configs
    mem_get_u32(artnet_handle, "ip_addr", &n->state.ip_addr.s_addr);
    mem_get_u32(artnet_handle, "net_mask", &n->state.net_mask.s_addr);
    mem_get_u8(artnet_handle, "dhcp", &n->state.dhcp);
    mem_get_u16(artnet_handle, "port", &n->state.port);

    // name configs
    mem_get_str(artnet_handle, "short_name", &n->state.short_name, ARTNET_SHORT_NAME_LENGTH);
    mem_get_str(artnet_handle, "long_name", &n->state.long_name, ARTNET_LONG_NAME_LENGTH);

    // artnet configs
    mem_get_u8(artnet_handle, "net", &n->state.net);
    mem_get_u8(artnet_handle, "subnet", &n->state.subnet);

}

void save_node_configs(artnet_node n) {
    // TO CHANGE: check err returned by nvs_set_xxx()

    ESP_LOGI(TAG, "Saving node configs to NVS");

    // network configs
    nvs_set_u32(artnet_handle, "ip_addr", n->state.ip_addr.s_addr);
    nvs_set_u32(artnet_handle, "net_mask", n->state.net_mask.s_addr);
    nvs_set_u8(artnet_handle, "dhcp", n->state.dhcp);
    nvs_set_u16(artnet_handle, "port", n->state.port);

    // name configs

    nvs_set_str(artnet_handle, "short_name", n->state.short_name);
    nvs_set_str(artnet_handle, "long_name", n->state.long_name);

    // artnet configs
    nvs_set_u8(artnet_handle, "net", n->state.net);
    nvs_set_u8(artnet_handle, "subnet", n->state.subnet);

    nvs_commit(artnet_handle);
}

// led matrix configs
uint8_t load_dimmer_value() {
    uint8_t dimmer = 100; // defaulting to max
    mem_get_u8(system_handle, "dimmer", &dimmer);
    return dimmer;
}

void store_dimmer_value(uint8_t dimmer) {

    ESP_LOGI(TAG, "Storing dimmer value: %d", dimmer);

    nvs_set_u8(system_handle, "dimmer", dimmer);
    nvs_commit(system_handle);
}


static void mem_get_u8(nvs_handle_t handle, char *key, void *to) {

    esp_err_t err = nvs_get_u8(handle, key, to);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGW(TAG, "%s not found in NVS", key);
    }
    else if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error getting %s from NVS", key);
    }
    else {
        ESP_LOGD(TAG, "Successfully got %s from NVS", key);
    }
}

static void mem_get_u16(nvs_handle_t handle, char *key, void *to) {

    esp_err_t err = nvs_get_u16(handle, key, to);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGW(TAG, "%s not found in NVS", key);
    }
    else if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error getting %s from NVS", key);
    }
    else {
        ESP_LOGD(TAG, "Successfully got %s from NVS", key);
    }
}

static void mem_get_u32(nvs_handle_t handle, char *key, void *to) {

    esp_err_t err = nvs_get_u32(handle, key, to);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGW(TAG, "%s not found in NVS", key);
    }
    else if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error getting %s from NVS", key);
    }
    else {
        ESP_LOGD(TAG, "Successfully got %s from NVS", key);
    }
}

static void mem_get_str(nvs_handle_t handle, char *key, void *to, size_t length) {

    size_t len = length;
    char *str = malloc(len);
    memset(str, 0x00, len);

    esp_err_t err = nvs_get_str(handle, key, str, &len);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGW(TAG, "%s not found in NVS", key);
    }
    else if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error getting %s from NVS", key);
    }
    else {
        ESP_LOGD(TAG, "Successfully got %s from NVS", key);
        memset(to, 0x00, length);
        strcpy(to, str);
    }
    free(str);
}
