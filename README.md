# ESP32 Artnet Matrix

An esp-32 dev board connected to an LED Matrix which supports Art-Net

- Built using esp-idf v4.4
- Using an ESP32-DevKitC (with an ESP32-WROOM-32D MCU)
- WS2812b 8x8 LED matrix


## Design
- A reimplementation of [libartnet](https://github.com/OpenLightingProject/libartnet) with a couple of tweaks:
	- Added support for Art-Net 4
	- Specifics to an esp-32
	- Added support for other packet types useful for this project (ArtIpProg, ArtCommand, ...)

- RMT task to drive the LED matrix is heavily inspired from [this](https://github.com/Lucas-Bruder/ESP32_LED_STRIP).


## TODO
- Handle ports correctly
- Implement OTA using the IDF API and through the Art-Net firmware packets.



## Inspirations / References
- To add sACN one day:
	- [https://github.com/Hundemeier/sacn](https://github.com/vanvught/rpidmx512)
	- [https://github.com/ETCLabs/sACN](https://github.com/vanvught/rpidmx512) (popular implementation)
	- [https://github.com/vanvught/rpidmx512](https://github.com/vanvught/rpidmx512) (interesting stuff with their libs (both for Art-Net and sACN)
- Led drivers:
	- Using [RMT](https://github.com/Lucas-Bruder/ESP32_LED_STRIP) (Current)
	- Using [I2S](https://github.com/8-DK/ESP32_I2S_WS2812_idf)
	- Using [SPI](https://github.com/8-DK/ESP32_SPI_WS2812_idf)
	- [FastLED port to idf](https://github.com/bbulkow/FastLED-idf) (supporting RMT and I2S)
	- Other RMT and SPI implementations: [esp-idf-lib](https://github.com/UncleRus/esp-idf-lib)


- Cool docs:
	- Art-Net 4 specs: [https://art-net.org.uk/resources/art-net-specification](https://art-net.org.uk/resources/art-net-specification) 
	- [RBoot](https://github.com/raburton/rboot) (an open source implementation of an ESP8266 bootloader) + the author's [blog](https://richard.burtons.org/2015/05/)




## Issues
See [ISSUES.md](./ISSUES.md)