#define LOG_LOCAL_LEVEL     ESP_LOG_DEBUG

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include "wifi.h"
#include "udp_server.h"
#include "common.h"

/* --------- Local Defines --------- */
// Set through menuconfig
#define ESP_WIFI_SSID      CONFIG_ESP_WIFI_SSID
#define ESP_WIFI_PASS      CONFIG_ESP_WIFI_PASSWORD
#define ESP_MAXIMUM_RETRY  CONFIG_ESP_MAXIMUM_RETRY
#define ESP_WIFI_HOSTNAME  CONFIG_ESP_WIFI_HOSTNAME

// Event group bits
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
#define IP_SET_BIT         BIT0
#define IP_FAIL_BIT        BIT1


/* --------- Local Variables --------- */
static const char *TAG = "wifi station";

static int s_retry_num = 0;
static esp_netif_t *esp_netif;

// FreeRTOS event group to signal when we are connected
static EventGroupHandle_t s_wifi_event_group;
static EventGroupHandle_t s_ip_event_group;


/* --------- Local Functions --------- */
static void set_static_ip(esp_netif_t *netif);
static void set_dhcp_ip(esp_netif_t *netif);
static void set_bcast_addr();

static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
static void ip_event_handler(void * arg, esp_event_base_t event_base, int32_t event_id, void* event_data);




static void set_static_ip(esp_netif_t *netif) {
    if (esp_netif_dhcpc_stop(netif) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to stop dhcp client");
        xEventGroupSetBits(s_ip_event_group, IP_FAIL_BIT);
        return;
    }

    esp_netif_ip_info_t ip;
    memset(&ip, 0 , sizeof(esp_netif_ip_info_t));

    ip.ip.addr = node->state.ip_addr.s_addr;
    ip.netmask.addr = node->state.net_mask.s_addr;
    ip.gw.addr = ipaddr_addr("127.0.0.1");  // not ideal but otherwise the function never triggers an event
    

    if (esp_netif_set_ip_info(netif, &ip) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to set ip info");
        xEventGroupSetBits(s_ip_event_group, IP_FAIL_BIT);
    }
}


static void set_dhcp_ip(esp_netif_t *netif) {
    esp_netif_dhcp_status_t status; 
    esp_netif_dhcpc_get_status(netif, &status);

    if (status == ESP_NETIF_DHCP_STOPPED) {
        ESP_LOGD(TAG, "Starting DHCP client");
        if (esp_netif_dhcpc_start(netif) != ESP_OK) {
            ESP_LOGE(TAG, "Failed to start dhcp client");
            xEventGroupSetBits(s_ip_event_group, IP_FAIL_BIT);
        }
    }
    else {
        ESP_LOGD(TAG, "DHCP server already started, skipping");
        xEventGroupSetBits(s_ip_event_group, IP_SET_BIT);
    }

}


static void set_bcast_addr() {
    uint32_t net_ip = 0;
    uint32_t bcast_ip = 0;
    uint32_t ip = node->state.ip_addr.s_addr;
    uint32_t mask = node->state.net_mask.s_addr;

    net_ip = ip & mask;
    ESP_LOGD(TAG, "Network ip: " IPSTR, IP2STR((esp_ip4_addr_t *) &net_ip));
    mask = ~mask;
    ESP_LOGD(TAG, "Not mask: " IPSTR, IP2STR((esp_ip4_addr_t *) &mask));

    bcast_ip = (net_ip | mask);
    ESP_LOGD(TAG, "Broadcast: " IPSTR, IP2STR((esp_ip4_addr_t *) &bcast_ip));
    node->state.bcast_addr.s_addr = bcast_ip;
}


static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
    if (event_base == WIFI_EVENT) {
        if (event_id == WIFI_EVENT_STA_START) {
            esp_wifi_connect();
        }
        else if (event_id == WIFI_EVENT_STA_DISCONNECTED) {
            if (s_retry_num < ESP_MAXIMUM_RETRY) {
                esp_wifi_connect();
                s_retry_num++;
                ESP_LOGI(TAG, "retry to connect to the AP");
            } else {
                ESP_LOGW(TAG, "connect to the AP fail");
                xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
            }
        }
        else if (event_id == WIFI_EVENT_STA_CONNECTED) {
            if (node->state.dhcp == false) {
                set_static_ip(arg);
            }
            s_retry_num = 0;
            xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        }
    }
}


static void ip_event_handler(void * arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {

    if (event_base == IP_EVENT) {
        if (event_id == IP_EVENT_STA_LOST_IP) {
            ESP_LOGE(TAG, "Lost ip");
        }
        else if (event_id == IP_EVENT_STA_GOT_IP) {
            ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
            ESP_LOGI(TAG, "Got ip: " IPSTR, IP2STR(&event->ip_info.ip));
            ESP_LOGD(TAG, "ip changed status %d", event->ip_changed);

            if (node->state.dhcp == true) {
                node->state.ip_addr.s_addr = event->ip_info.ip.addr;  // setting node state
                node->state.net_mask.s_addr = event->ip_info.netmask.addr; 
            } // not setting ip if DHCP is disabled since it means we got the ip from the node config

            xEventGroupSetBits(s_ip_event_group, IP_SET_BIT);
        }
    }
}


esp_err_t set_ip_type() {

    esp_err_t err;

    ESP_LOGD(TAG, "Setting new ip type");

    s_ip_event_group = xEventGroupCreate();

    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &ip_event_handler, esp_netif, &instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_LOST_IP, &ip_event_handler, esp_netif, &instance_got_ip));

    if (node->state.dhcp == true) {
        set_dhcp_ip(esp_netif);
    }
    else {
        set_static_ip(esp_netif);
    }

    // Waiting for event
    EventBits_t ip_bits = xEventGroupWaitBits(s_ip_event_group,
        IP_SET_BIT | IP_FAIL_BIT,
        pdTRUE,
        pdFALSE,
        1000);

    if (ip_bits & IP_SET_BIT) {
        ESP_LOGI(TAG, "Successfully set IP");
        err = ESP_OK;
        set_bcast_addr();
    } else if (ip_bits & IP_FAIL_BIT) {
        ESP_LOGE(TAG, "Failed to set ip");
        err = ESP_FAIL;
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
        err = ESP_FAIL;
    }

    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_LOST_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    vEventGroupDelete(s_ip_event_group);

    ESP_LOGD(TAG, "Done");

    return err;
}


void init_wifi_sta(void) {
    s_wifi_event_group = xEventGroupCreate();
    s_ip_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif = esp_netif_create_default_wifi_sta();

    ESP_ERROR_CHECK(esp_netif_set_hostname(esp_netif, node->state.short_name));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        esp_netif,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &ip_event_handler,
                                                        esp_netif,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = ESP_WIFI_SSID,
            .password = ESP_WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t wifi_bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    EventBits_t ip_bits = xEventGroupWaitBits(s_ip_event_group,
            IP_SET_BIT | IP_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (wifi_bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s using Hostname:%s", ESP_WIFI_SSID, node->state.short_name);
    } else if (wifi_bits & WIFI_FAIL_BIT) {
        ESP_LOGW(TAG, "Failed to connect to SSID:%s", ESP_WIFI_SSID);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    if (ip_bits & IP_SET_BIT) {
        ESP_LOGI(TAG, "Successfully set IP");
        set_bcast_addr();
    } else if (ip_bits & IP_FAIL_BIT) {
        ESP_LOGE(TAG, "Failed to set ip");
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);
    vEventGroupDelete(s_ip_event_group);
}