#ifndef UDPSERVER_H
#define UDPSERVER_H

#include "freertos/event_groups.h"

#include "packets.h"

extern EventGroupHandle_t s_server_event_group;
#define SERVER_PORT_CHANGE_BIT  BIT0


void udp_server_task(void *pvParameters);
int udp_server_send(artnet_packet p);

#endif