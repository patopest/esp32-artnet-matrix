#define LOG_LOCAL_LEVEL     ESP_LOG_DEBUG

#include <string.h>
#include <sys/param.h>

#include "esp_log.h"
#include "esp_netif.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include "udp_server.h"
#include "led.h"
#include "packets.h"
#include "receive.h"
#include "common.h"

/* --------- Global Variables --------- */
EventGroupHandle_t s_server_event_group;


/* --------- Local Variables --------- */
static const char *TAG = "udp server";

uint8_t MIN_PACKET_SIZE = 10;
int sock;



void udp_server_task(void *pvParameters) {
    // char rx_buffer[128];
    artnet_packet_t p;
    char addr_str[128];
    int addr_family = (int)pvParameters;
    int ip_protocol = 0;
    struct sockaddr_in6 dest_addr;

    s_server_event_group = xEventGroupCreate();

    while (1) {

        if (addr_family == AF_INET) {
            struct sockaddr_in *dest_addr_ip4 = (struct sockaddr_in *)&dest_addr;
            dest_addr_ip4->sin_addr.s_addr = htonl(INADDR_ANY);
            dest_addr_ip4->sin_family = AF_INET;
            dest_addr_ip4->sin_port = htons(node->state.port);
            ip_protocol = IPPROTO_IP;
        } else if (addr_family == AF_INET6) {   // probably to remove later
            bzero(&dest_addr.sin6_addr.un, sizeof(dest_addr.sin6_addr.un));
            dest_addr.sin6_family = AF_INET6;
            dest_addr.sin6_port = htons(node->state.port);
            ip_protocol = IPPROTO_IPV6;
        }

        sock = socket(addr_family, SOCK_DGRAM, ip_protocol);
        if (sock < 0) {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            break;
        }
        ESP_LOGD(TAG, "Socket created");

        int true_flag = 1;
        if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &true_flag, sizeof(true_flag)) < 0) {
            ESP_LOGE(TAG, "Failed to set sock options: errno %d", errno);
            break;
        }

        int err = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err < 0) {
            ESP_LOGE(TAG, "Socket unable to bind: errno %d", errno);
        }
        ESP_LOGI(TAG, "Socket bound, port %d", node->state.port);

        while (1) {

            EventBits_t bits = xEventGroupGetBits(s_server_event_group);
            if (bits & SERVER_PORT_CHANGE_BIT) {
                xEventGroupClearBits(s_server_event_group, SERVER_PORT_CHANGE_BIT);
                ESP_LOGD(TAG, "Port changed, breaking out of loop");
                break;
            }

            memset(&p.data, 0x0, sizeof(p.data));

            ESP_LOGD(TAG, "Waiting for data");
            struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
            socklen_t socklen = sizeof(source_addr);
            int len = recvfrom(sock, (char *) &(p.data), sizeof(p.data), 0, (struct sockaddr *)&source_addr, &socklen);

            // Error occurred during receiving
            if (len < 0) {
                ESP_LOGE(TAG, "recvfrom failed: errno %d", errno);
                break;
            }
            // Data received
            else {
                p.length = len;

                // Get the sender's ip address as string
                if (source_addr.ss_family == PF_INET) {
                    inet_ntoa_r(((struct sockaddr_in *)&source_addr)->sin_addr, addr_str, sizeof(addr_str) - 1);
                } else if (source_addr.ss_family == PF_INET6) {
                    inet6_ntoa_r(((struct sockaddr_in6 *)&source_addr)->sin6_addr, addr_str, sizeof(addr_str) - 1);
                }

                // ((struct sockaddr_in *) &source_addr)->sin_addr

                memcpy(&(p.from), &(((struct sockaddr_in *) &source_addr)->sin_addr), sizeof(struct in_addr));
                p.port = ntohs(((struct sockaddr_in *) &source_addr)->sin_port);

                ESP_LOGD(TAG, "\n");
                ESP_LOGI(TAG, "Received %d bytes from %s", len, addr_str);

                // int err = sendto(sock, rx_buffer, len, 0, (struct sockaddr *)&source_addr, sizeof(source_addr));
                // if (err < 0) {
                //     ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
                //     break;
                // }

                if (p.length > MIN_PACKET_SIZE && get_type(&p)) {
                    handle(&p);
                }
                else {
                    node->state.report_code = ARTNET_RCPARSEFAIL;
                }

            }
        }

        if (sock != -1) {
            ESP_LOGI(TAG, "Shutting down socket and restarting...");
            shutdown(sock, 0);
            close(sock);
        }
    }
    vEventGroupDelete(s_server_event_group);
    vTaskDelete(NULL);
}


int udp_server_send(artnet_packet p) {
    struct sockaddr_in addr;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(p->port);
    addr.sin_addr = p->to;
    p->from = node->state.ip_addr;

    ESP_LOGI(TAG, "Sending to %s" , inet_ntoa(addr.sin_addr));

    int err = sendto(sock, (char *) &p->data, p->length, 0, (struct sockaddr *)&addr, sizeof(addr));
    if (err < 0) {
        ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
        node->state.report_code = ARTNET_RCUDPFAIL;
        return 1;
    }
    else if (p->length != err) {
        ESP_LOGE(TAG, "Error sending full datagram: errno %d", errno);
        node->state.report_code = ARTNET_RCSOCKETWR1;
        return 2;
    }
    return 0;
}

