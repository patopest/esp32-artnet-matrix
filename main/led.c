#define LOG_LOCAL_LEVEL     ESP_LOG_DEBUG

#include <string.h>
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "driver/rmt.h"

#include "led.h"
#include "memory.h"

// Common
#define LED_PIN 	            (19)
// GPIO specifics
#define GPIO_OUTPUT_PIN_SEL     (1ULL<<LED_PIN)
// Ledc (PWM) specifics
#define LEDC_TIMER              LEDC_TIMER_0
#define LEDC_MODE               LEDC_LOW_SPEED_MODE
#define LEDC_CHANNEL            LEDC_CHANNEL_0
#define LEDC_DUTY_RES           LEDC_TIMER_8_BIT // Set duty resolution to 8 bits
#define LEDC_FREQUENCY          (5000) // Frequency in Hertz. Set frequency at 5 kHz
// WS2812B specifics
#define NUM_LEDS                (64)
#define RMT_TX_GPIO_PIN         (25)
#define RMT_TX_CHANNEL          RMT_CHANNEL_0
#define RMT_CLOCK_DIV           (4)   // clock is at 80MHz -> so here 20MHz (25ns period)
#define LEDS_REFRESH_PERIOD     (33U) // 30 fps
#define LEDS_RESOLUTION         (24U) // 3*8 bits = 24 bits LEDs
#define WS2812_BIT_1_HIGH       (18)  // (900ns +/- 150ns per datasheet)
#define WS2812_BIT_1_LOW        (7)   // (350ns +/- 150ns per datasheet)
#define WS2812_BIT_0_HIGH       (7)   // (350ns +/- 150ns per datasheet)
#define WS2812_BIT_0_LOW        (18)  // (900ns +/- 150ns per datasheet)

/* --------- Local Variables --------- */
static const char *TAG = "led";

/* --------- Local Functions --------- */
static void fill_rmt_items(struct led_color_s *buf, rmt_item32_t *rmt_items, uint16_t length);
static void ws2812_rmt_bit_1(rmt_item32_t* item);
static void ws2812_rmt_bit_0(rmt_item32_t* item);


/* ---------- GPIO led --------- */
void init_gpio(){
	//zero-initialize the config structure.
    gpio_config_t io_conf = {};
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    ESP_ERROR_CHECK(gpio_config(&io_conf));

}

void gpio_set(uint32_t level){
	ESP_LOGI(TAG, "Setting gpio pin %d to %d", LED_PIN, level);
	ESP_ERROR_CHECK(gpio_set_level(LED_PIN, level));
}


/* ---------- PWM led ---------- */
void init_ledc() {
	// Prepare and then apply the LEDC PWM timer configuration
    ledc_timer_config_t ledc_timer = {
        .speed_mode       = LEDC_MODE,
        .timer_num        = LEDC_TIMER,
        .duty_resolution  = LEDC_DUTY_RES,
        .freq_hz          = LEDC_FREQUENCY,  // Set output frequency at 5 kHz
        .clk_cfg          = LEDC_AUTO_CLK
    };
    ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

    // Prepare and then apply the LEDC PWM channel configuration
    ledc_channel_config_t ledc_channel = {
        .speed_mode     = LEDC_MODE,
        .channel        = LEDC_CHANNEL,
        .timer_sel      = LEDC_TIMER,
        .intr_type      = LEDC_INTR_DISABLE,
        .gpio_num       = LED_PIN,
        .duty           = 0, // Set duty to 0%
        .hpoint         = 0
    };
    ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
}

void led_set(uint32_t level) {
	ESP_ERROR_CHECK(ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, level));
	ESP_ERROR_CHECK(ledc_update_duty(LEDC_MODE, LEDC_CHANNEL));
}



/* ---------- WS2812B leds ---------- */
led_strip init_led_strip() {

    led_strip led_strip = malloc(sizeof(led_strip_t));

    led_strip->rmt_channel = RMT_TX_CHANNEL;
    led_strip->gpio_pin = RMT_TX_GPIO_PIN;
    led_strip->length = NUM_LEDS;
    led_strip->dimmer = load_dimmer_value();
    led_strip->showing_buf_1 = true;
    led_strip->buffer_1 = malloc(sizeof(struct led_color_s) * NUM_LEDS);
    led_strip->buffer_2 = malloc(sizeof(struct led_color_s) * NUM_LEDS);
    led_strip->access_semaphore = xSemaphoreCreateBinary();

    memset(led_strip->buffer_1, 0, sizeof(struct led_color_s) * led_strip->length);
    memset(led_strip->buffer_2, 0, sizeof(struct led_color_s) * led_strip->length);

    xSemaphoreGive(led_strip->access_semaphore);

    rmt_config_t config = {
        .rmt_mode = RMT_MODE_TX,
        .channel = led_strip->rmt_channel,
        .gpio_num = led_strip->gpio_pin,
        .clk_div = RMT_CLOCK_DIV,
        .mem_block_num = 1,
        .flags = 0,
        .tx_config = {
            .carrier_en = false,
            .carrier_freq_hz = 100, // not used but need to be set to avoid errors
            .carrier_level = RMT_CARRIER_LEVEL_LOW,
            .carrier_duty_percent = 50,
            .loop_en = false,
            .idle_output_en = true,
            .idle_level = RMT_IDLE_LEVEL_LOW,
        } 
    };

    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));

    uint32_t counter = 0;
    rmt_get_counter_clock(led_strip->rmt_channel, &counter);
    ESP_LOGD(TAG, "Counter clock: %d Hz", counter);

    return led_strip;
}

void deinit_led_strip(led_strip led_strip) {
    ESP_LOGD(TAG, "Deinit led_strip");
    free(led_strip->buffer_1);
    free(led_strip->buffer_2);
    vSemaphoreDelete(led_strip->access_semaphore);
    free(led_strip);
}

void led_strip_task(void *pvParameters) {

    led_strip led_strip = (led_strip_t *) pvParameters;
    bool make_new_rmt_items = true;
    bool prev_showing_buf_1 = !led_strip->showing_buf_1;

    size_t num_rmt_items = (LEDS_RESOLUTION * led_strip->length);
    rmt_item32_t *rmt_items = (rmt_item32_t*) malloc(sizeof(rmt_item32_t) * num_rmt_items);
    
    if (!rmt_items) {
        vTaskDelete(NULL);
    }

    ESP_LOGI(TAG, "Starting led_strip task");

    while (1) {
        rmt_wait_tx_done(led_strip->rmt_channel, 0);
        xSemaphoreTake(led_strip->access_semaphore, portMAX_DELAY);

         /*
         * If buf 1 was previously being shown and now buf 2 is being shown,
         * it should update the new rmt items array. If buf 2 was previous being shown
         * and now buf 1 is being shown, it should update the new rmt items array.
         * Otherwise, no need to update the array
         */
        if ((prev_showing_buf_1 == true) && (led_strip->showing_buf_1 == false)) {
            make_new_rmt_items = true;
        } else if ((prev_showing_buf_1 == false) && (led_strip->showing_buf_1 == true)) {
            make_new_rmt_items = true;
        } else {
            make_new_rmt_items = false;
        }

        if (make_new_rmt_items) {
            ESP_LOGD(TAG, "make new rtm items");
            if (led_strip->showing_buf_1) {
                fill_rmt_items(led_strip->buffer_1, rmt_items, led_strip->length);
            } else {
                fill_rmt_items(led_strip->buffer_2, rmt_items, led_strip->length);
            }
        }


        rmt_write_items(led_strip->rmt_channel, rmt_items, num_rmt_items, false);
        prev_showing_buf_1 = led_strip->showing_buf_1;
        xSemaphoreGive(led_strip->access_semaphore);

        vTaskDelay(LEDS_REFRESH_PERIOD / portTICK_PERIOD_MS);
    }

    free(rmt_items);
    vTaskDelete(NULL);
}

static void fill_rmt_items(struct led_color_s *buf, rmt_item32_t *rmt_items, uint16_t length) {

    uint16_t rmt_items_index = 0;
    for (uint32_t led_index = 0; led_index < length; led_index++) {
        struct led_color_s led_color = buf[led_index];

        for (uint8_t bit = 8; bit != 0; bit--) {
            uint8_t bit_set = (led_color.green >> (bit - 1)) & 1;
            if(bit_set) {
                ws2812_rmt_bit_1(&(rmt_items[rmt_items_index]));
            } else {
                ws2812_rmt_bit_0(&(rmt_items[rmt_items_index]));
            }
            rmt_items_index++;
        }
        for (uint8_t bit = 8; bit != 0; bit--) {
            uint8_t bit_set = (led_color.red >> (bit - 1)) & 1;
            if(bit_set) {
                ws2812_rmt_bit_1(&(rmt_items[rmt_items_index]));
            } else {
                ws2812_rmt_bit_0(&(rmt_items[rmt_items_index]));
            }
            rmt_items_index++;
        }
        for (uint8_t bit = 8; bit != 0; bit--) {
            uint8_t bit_set = (led_color.blue >> (bit - 1)) & 1;
            if(bit_set) {
                ws2812_rmt_bit_1(&(rmt_items[rmt_items_index]));
            } else {
                ws2812_rmt_bit_0(&(rmt_items[rmt_items_index]));
            }
            rmt_items_index++;
        }
    }
}

static void ws2812_rmt_bit_1(rmt_item32_t* item) {
    item->level0 = 1;
    item->duration0 = WS2812_BIT_1_HIGH;
    item->level1 = 0;
    item->duration1 = WS2812_BIT_1_LOW;
}

static void ws2812_rmt_bit_0(rmt_item32_t* item) {
    item->level0 = 1;
    item->duration0 = WS2812_BIT_0_HIGH;
    item->level1 = 0;
    item->duration1 = WS2812_BIT_0_LOW;
}

void set_leds(led_strip led_strip, uint8_t *buffer, uint8_t num_leds) {

    ESP_LOGD(TAG, "Setting buffer %d", led_strip->showing_buf_1);
    for (uint8_t i = 0; i < num_leds; i++) {
        uint16_t index = 3 * i;
        if (led_strip->showing_buf_1) {
            led_strip->buffer_2[i].red = ((buffer[index] *led_strip->dimmer) / 100);
            led_strip->buffer_2[i].green = ((buffer[index+1] *led_strip->dimmer) / 100);
            led_strip->buffer_2[i].blue = ((buffer[index+2] *led_strip->dimmer) / 100);
        }
        else {
            led_strip->buffer_1[i].red = ((buffer[index] *led_strip->dimmer) / 100);
            led_strip->buffer_1[i].green = ((buffer[index+1] *led_strip->dimmer) / 100);
            led_strip->buffer_1[i].blue = ((buffer[index+2] *led_strip->dimmer) / 100);
        }
    }

}

void set_led(led_strip led_strip, uint8_t pos, uint8_t red, uint8_t green, uint8_t blue) {
    ESP_LOGD(TAG, "setting led %d at [%d,%d,%d]", pos, red, green, blue);
    if (led_strip->showing_buf_1) {
        led_strip->buffer_2[pos].red = red;
        led_strip->buffer_2[pos].green = green;
        led_strip->buffer_2[pos].blue = blue;
    }
    else {
        led_strip->buffer_1[pos].red = red;
        led_strip->buffer_1[pos].green = green;
        led_strip->buffer_1[pos].blue = blue;
    }
}

void clear_leds(led_strip led_strip) {
    ESP_LOGD(TAG, "Clearing leds");
    if (led_strip->showing_buf_1) {
        memset(led_strip->buffer_2, 0, sizeof(struct led_color_s) * led_strip->length);
    }
    else {
        memset(led_strip->buffer_1, 0, sizeof(struct led_color_s) * led_strip->length);
    }
}

void flush_leds(led_strip led_strip) {

    xSemaphoreTake(led_strip->access_semaphore, portMAX_DELAY);
    ESP_LOGD(TAG, "Flushing led values in buffer %d", led_strip->showing_buf_1);
    if (led_strip->showing_buf_1) {
        led_strip->showing_buf_1 = false;
        memset(led_strip->buffer_1, 0, sizeof(struct led_color_s) * led_strip->length);
    } else {
        led_strip->showing_buf_1 = true;
        memset(led_strip->buffer_2, 0, sizeof(struct led_color_s) * led_strip->length);
    }
    xSemaphoreGive(led_strip->access_semaphore);

}
