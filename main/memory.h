#ifndef MEMORY_H
#define MEMORY_H

#include "common.h"

void init_memory();
void set_restart_counter();
uint32_t get_restart_counter();

// artnet node configs
void load_node_configs(artnet_node n);
void save_node_configs(artnet_node n);

// led matrix configs
uint8_t load_dimmer_value();
void store_dimmer_value(uint8_t dimmer);


#endif