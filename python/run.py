from stupidArtnet import StupidArtnet
import time

target_ip = "192.168.1.112"
universe = 0 						
packet_size = 192


a = StupidArtnet(target_ip, universe, packet_size, fps=30, even_packet_size=True, broadcast=True)
print(a)

# if we want to thread (also do a.stop() when done)
# a.start() 


packet = bytearray(packet_size)
for i in range(packet_size):
    # packet[i] = (i % 256)
    packet[i] = 0
    # if (i % 3 == 0):
    #     packet[i] = i
    # elif (i % 3 == 1):
    #     packet[i] = 0
    # elif (i % 3 == 2):
    #     packet[i] = 0

print(packet)

a.set(packet)

# Send data
print("Sending data...")
a.show()

print("Data sent")

# a.stop()




# Note: why is the lib sending a blackout packet when .start() 