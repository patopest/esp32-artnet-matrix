#ifndef GPIO_H
#define GPIO_H

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include <driver/rmt.h>
#include <driver/gpio.h>


/* ---------- GPIO led --------- */
void init__gpio();
void gpio_set(uint32_t level);

/* ---------- PWM led ---------- */
void init_ledc();
void led_set(uint32_t level);

/* ---------- WS2812B leds ---------- */
struct led_color_s {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

typedef struct led_strip_s {
    rmt_channel_t rmt_channel;
    gpio_num_t gpio_pin;
    uint16_t length;
    uint8_t dimmer;    // as percentage in range (0-100)

    bool showing_buf_1;
    bool new_buffer_available;
    struct led_color_s *buffer_1;
    struct led_color_s *buffer_2;

    SemaphoreHandle_t access_semaphore;
} led_strip_t;

typedef led_strip_t *led_strip;

/** The LED matrix **/
extern led_strip led_matrix;

led_strip init_led_strip();
void deinit_led_strip(led_strip led_strip);
void led_strip_task(void *pvParameters);
void set_leds(led_strip led_strip, uint8_t *buffer, uint8_t num_leds);
void set_led(led_strip led_strip, uint8_t pos, uint8_t red, uint8_t green, uint8_t blue);
void clear_leds(led_strip led_strip);
void flush_leds(led_strip led_strip);


#endif