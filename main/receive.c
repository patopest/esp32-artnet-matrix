#define LOG_LOCAL_LEVEL		ESP_LOG_DEBUG
#include <string.h>
#include "esp_log.h"
#include "esp_system.h"

#include "common.h"
#include "wifi.h"
#include "udp_server.h"
#include "receive.h"
#include "transmit.h"
#include "memory.h"
#include "led.h"

static const char *TAG = "artnet receive";

/* --------- Local Functions --------- */
static void handle_dmx(artnet_packet p);
static void handle_poll(artnet_packet p);
static void handle_ipprog(artnet_packet p);
static void handle_address(artnet_packet p);
static void handle_command(artnet_packet p);



/*
 * handle an ArtDmx packet
 */
static void handle_dmx(artnet_packet p) {

	ESP_LOGD(TAG, "received dmx 0: %d",p->data.admx.data[0]);
	// PWM led
	// led_set(p->data.admx.data[0]);

	// WS2812B led matrix
	set_leds(led_matrix, p->data.admx.data, led_matrix->length);
	flush_leds(led_matrix);
}

/*
 * handle an ArtPoll packet
 */
static void handle_poll(artnet_packet p) {	

	// if node you send Diagnostic messages
	if (p->data.ap.ttm & TTM_DIAG_TX) {
		node->state.send_diag = true;

		// diagnostics unicast or broadcast
		if (p->data.ap.ttm & TTM_DIAG_TYPE) {
			node->state.diag_unicast = true;
			node->state.reply_addr = p->from;
		}
		else {
			node->state.diag_unicast = false;
		}

		// diagnostics priority
		// not checking if priority code is a correct defined value
		node->state.diag_priority = p->data.ap.priority;
	}
	else {
		node->state.send_diag = false;
	}

  // if we are told to send updates when node conditions change
  if (p->data.ap.ttm & TTM_ONCHANGE_SEND) {
    node->state.send_apr_on_change = true;
  }
  else {
    node->state.send_apr_on_change = false;
  }


	artnet_tx_poll_reply(node, p, true);
}

/*
 * handle an ArtIpProg packet
 */
static void handle_ipprog(artnet_packet p) {

	bool reset_netif = true;
	bool state_changed = false;

	ESP_LOGD(TAG, "command is 0x%x", p->data.aip.command);

	// if we are told to program
	if (p->data.aip.command & IPPROG_PROGRAM) {

		// if set DHCP is enabled, ignore the rest of the bits
		if (p->data.aip.command & IPPROG_DHCP) {
			if (node->state.dhcp == false) {
				state_changed = true;
			}
			node->state.dhcp = true;
		}
		else {
			if (p->data.aip.command & IPPROG_DEFAULT) {
				set_node_default_network_config(node);
				xEventGroupSetBits(s_server_event_group, SERVER_PORT_CHANGE_BIT);
			}
			else {
				if (p->data.aip.command & IPPROG_IP) {
					node->state.dhcp = false;
					memcpy(&node->state.ip_addr.s_addr, &p->data.aip.progIp, 4);
					ESP_LOGD(TAG, "Program ip to %s", inet_ntoa(node->state.ip_addr.s_addr));
				}
				if (p->data.aip.command & IPPROG_NETMASK) {
					node->state.dhcp = false;
					memcpy(&node->state.net_mask.s_addr, &p->data.aip.progSm, 4);
					ESP_LOGD(TAG, "Program netmask to %s", inet_ntoa(node->state.net_mask.s_addr));
				}
				if (p->data.aip.command & IPPROG_PORT) {
					node->state.port = ntohs(p->data.aip.progPort);
					ESP_LOGD(TAG, "Program port to %d", node->state.port);

					// setting event bit for server to reinit socket
					xEventGroupSetBits(s_server_event_group, SERVER_PORT_CHANGE_BIT);
					reset_netif = false;
				}
			}
		}

		if (reset_netif == true) {
			set_ip_type();   // set new node network config
		}

		artnet_tx_ipprog_reply(node, p);

		// check if state has changed
		state_changed |= (memcmp(&node->state.ip_addr.s_addr, &p->data.aip.progIp, 4) != 0);
		state_changed |= (memcmp(&node->state.net_mask.s_addr, &p->data.aip.progSm, 4) != 0);
		state_changed |= (memcmp(&node->state.port, &p->data.aip.progPort, 2) != 0);

		if (state_changed) {
			artnet_tx_build_art_poll_reply(node); // rebuild buffered ArtPollReply packet

			if (node->state.send_apr_on_change == true) { //if set, send ArtPollReply on change
				ESP_LOGD(TAG, "state changed");
				artnet_tx_poll_reply(node, NULL, true);
				save_node_configs(node);  // store configs in memory
			}
		}
	}

}

/*
 * handle an ArtAddress packet
 */
static void handle_address(artnet_packet p) {

		// Net
		if (p->data.addr.net == 0x7f) {  // no change
			ESP_LOGD(TAG, "net: No change");
		}
		else if (p->data.addr.net & 0x80) { 	// bit 7 set, reprogram net
			ESP_LOGD(TAG, "net: reprogram to 0x%x", p->data.addr.net);
			node->state.net = (p->data.addr.net & 0x7F); // only keep bits 0 to 7
		}
		else if (p->data.addr.net == 0x00) { 	// reset net to default
			ESP_LOGD(TAG, "net: reset to default");
			node->state.net = 0;
		}

		// Subnet
		if (p->data.addr.subnet == 0x7f) {  // no change
			ESP_LOGD(TAG, "subnet: No change");
		}
		else if (p->data.addr.subnet & 0x80) { 	// bit 7 set, reprogram net
			ESP_LOGD(TAG, "subnet: reprogram to 0x%x", p->data.addr.subnet);
			node->state.subnet = (p->data.addr.subnet & 0xF); // only keep bits 0 to 4
		}
		else if (p->data.addr.subnet == 0x00) { 	// reset net to default
			ESP_LOGD(TAG, "subnet: reset to default");
			node->state.subnet = 0;
		}

		// short name
		if (p->data.addr.shortname[0] != 0) {
			ESP_LOGD(TAG, "short name: change to %s", p->data.addr.shortname);
			memcpy(&node->state.short_name, &p->data.addr.shortname, ARTNET_SHORT_NAME_LENGTH);
			node->state.report_code = ARTNET_RCSHNAMEOK;
			// Wifi hostname change will only reflect on interface restart (i.e.: node reboot)
		};

		// long name
		if (p->data.addr.longname[0] != 0) {
			ESP_LOGD(TAG, "long name: change to %s", p->data.addr.longname);
			memcpy(&node->state.long_name, &p->data.addr.longname, ARTNET_LONG_NAME_LENGTH);
			node->state.report_code = ARTNET_RCLONAMEOK;
		};

		// TO CHANGE: not handling SwIn and SwOut and Command since we only have one port....

		artnet_tx_build_art_poll_reply(node);
		artnet_tx_poll_reply(node, p, true);

		save_node_configs(node);  // store configs in memory
}

/*
 * handle an ArtAddress packet
 */
static void handle_command(artnet_packet p) {

	char str[1000];
	const char cmd_delim[2] = "&";
	const char data_delim[2] = "=";

	// only parse commands if ESTA codes match
	if ((p->data.acmd.estaman[0] == node->state.esta_hi) && (p->data.acmd.estaman[1] == node->state.esta_lo)) {
		ESP_LOGD(TAG, "received command (length %d): %s", ntohs(p->data.acmd.length), p->data.acmd.data);

		memcpy(&str, &p->data.acmd.data, ntohs(p->data.acmd.length)); // copying string because strtok() modifies it
		
		// parse commands (format is "command=data&...")
		char *token = strtok(str, data_delim);

		while (token != NULL) {
			ESP_LOGD(TAG, "cmd: %s", token);

			char *data = strtok(NULL, cmd_delim);
			ESP_LOGD(TAG, "data: %s", data);

			if (strcmp(token, "action") == 0) {
				if (strcmp(data, "reboot") == 0) {
					// reboot ESP32
					ESP_LOGI(TAG, "Received command to reboot");
					esp_restart();
				}
			}

			if (strcmp(token, "dimmer") == 0) {
				uint8_t dimmer = (uint8_t) atoi(data);
				ESP_LOGI(TAG, "Received command for dimmer at %d", dimmer);
				if (dimmer > 100) {
					ESP_LOGW(TAG, "Dimmer value is above 100, ignoring");
				}
				else {
					led_matrix->dimmer = dimmer;
					store_dimmer_value(dimmer); // writing to flash
				}
			}

			token = strtok(NULL, data_delim);
		}


	}


}

/*
 * The main handler for an artnet packet. calls
 * the appropriate handler function
 */
int handle(artnet_packet p) {

	switch (p->type) {
		case ARTNET_DMX:
			ESP_LOGI(TAG, "Received ArtDmx packet");
			handle_dmx(p);
			break;
		case ARTNET_POLL:
			ESP_LOGI(TAG, "Received ArtPoll packet");
			handle_poll(p);
			break;
		case ARTNET_REPLY:
			ESP_LOGI(TAG, "Received ArtPollReply packet");

			break;
		case ARTNET_IPPROG:
			ESP_LOGI(TAG, "Received ArtIpProg packet");
			handle_ipprog(p);
			break;
		case ARTNET_ADDRESS:
			ESP_LOGI(TAG, "Received ArtAddress packet");
			handle_address(p);
			break;
		case ARTNET_COMMAND:
			ESP_LOGI(TAG, "Received ArtCommand packet");
			handle_command(p);
			break;
		default:
			ESP_LOGI(TAG, "artnet but not yet implemented!, op was %x", (int) p->type);
	}

	return 0;
}


/**
 * this gets the opcode from a packet
 */
int16_t get_type(artnet_packet p) {
  uint8_t *data;

  if (p->length < 10)
    return 0;
  if (!memcmp(&p->data, "Art-Net\0", 8)) {
    // not the best here, this needs to be tested on different arch
    data = (uint8_t *) &p->data;

    p->type = (data[9] << 8) + data[8];
    return p->type;
  } else {
    return 0;
  }
}