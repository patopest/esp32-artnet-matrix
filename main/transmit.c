#define LOG_LOCAL_LEVEL		ESP_LOG_DEBUG
#include <string.h>
#include "esp_log.h"
#include "esp_system.h"

#include "transmit.h"
#include "udp_server.h"
#include "common.h"

static const char *TAG = "artnet transmit";

/*
 * Send an ArtPollReply packet
 */
int artnet_tx_poll_reply(artnet_node n, artnet_packet poll, bool response) {
	artnet_packet_t reply;
	artnet_reply_t *ar = (artnet_reply_t *) &reply.data;

	ESP_LOGI(TAG, "Building ArtPollReply packet");

	// copy from a poll reply template
  	memcpy(&reply.data, &n->ar_temp, sizeof(artnet_reply_t));

	if (!response && n->state.status == ARTNET_ON) {
    	n->state.reply_count++;  // increment reply counter
  	}

	if (response == true) {
		reply.to = poll->from;
		reply.port = poll->port;
	}
	else {
		reply.to = n->state.reply_addr;
		reply.port = ARTNET_PORT;
	}
	reply.type = ARTNET_REPLY;
	reply.length = sizeof(artnet_reply_t);

	// NodeReport
	snprintf((char *) &ar->nodereport, ARTNET_REPORT_LENGTH, "#%04x [%04d] Hello", n->state.report_code, n->state.reply_count);

	// will need to recompute status and ports here

	return udp_server_send(&reply);
}


/*
 * Send an ArtIpProgReply packet
 */
int artnet_tx_ipprog_reply(artnet_node n, artnet_packet ipprog) {
	artnet_packet_t reply;
	artnet_ipprog_reply_t *aipr = (artnet_ipprog_reply_t *) &reply.data;
	
	ESP_LOGI(TAG, "Building ArtIpProgReply packet");

	memset(aipr, 0x00, sizeof(artnet_ipprog_reply_t));

	reply.to = ipprog->from;  // using IpProg sender's address (not node's state.reply_addr) since reply should always be unicast to sender
	reply.port = ipprog->port;
	reply.type = ARTNET_IPREPLY;
	reply.length = sizeof(artnet_ipprog_reply_t);

	memcpy(&aipr->id, "Art-Net\0", 8);
	aipr->opCode = ARTNET_IPREPLY;
	aipr->proVerH = 0;
	aipr->proVer = ARTNET_VERSION;

	memcpy(&aipr->progIp, &n->state.ip_addr.s_addr, 4);
	memcpy(&aipr->progSm, &n->state.net_mask.s_addr, 4);
	aipr->progPort = htons(n->state.port);

	if (n->state.dhcp == true) {
		aipr->status = IPPROG_DHCP;
	}

	return udp_server_send(&reply);
}

/*
 * Send an ArtDiagData packet
 */
int artnet_tx_diagdata(artnet_node n, artnet_priority_code priority) {
	artnet_packet_t msg;
	artnet_diagdata_t *adiag = (artnet_diagdata_t *) &msg.data;

	ESP_LOGI(TAG, "Building ArtDiagData packet");

	// checking if diag messaged are enabled
	if (node->state.send_diag == false) {
		ESP_LOGW(TAG, "Node Diag messages are disabled");
		return 1;
	}
	// checking if node state allows to send this priority level of message
	if (priority < node->state.diag_priority) {
		ESP_LOGW(TAG, "Diag data attempt but node priority (%d) is higher than what is requested (%d).", node->state.diag_priority, priority);
		return 2;
	}

	memset(adiag, 0x00, sizeof(artnet_diagdata_t));

	if (node->state.diag_unicast == true) {
		msg.to = node->state.reply_addr;
	}
	else {
		msg.to = node->state.bcast_addr;
	}
	msg.port = ARTNET_PORT;
	msg.type = ARTNET_DIAGDATA;
	msg.length = sizeof(artnet_diagdata_t);

	memcpy(&adiag->id, "Art-Net\0", 8);
	adiag->opCode = ARTNET_DIAGDATA;
	adiag->proVerH = 0;
	adiag->proVer = ARTNET_VERSION;

	// message
	adiag->priority = priority;
	snprintf((char *) &adiag->data, ARTNET_MSG_LENGTH, "reboot:%d FM:v%d.%d", n->sys_state.restart_counter, FM_VERSION_MAJOR, FM_VERSION_MINOR);
	adiag->length = htons(strlen((char *) &adiag->data));

	// only one type of message for now. Will handle more later

	return udp_server_send(&msg);
}


// this is called when the node's state changes to rebuild the
// artpollreply packet
int artnet_tx_build_art_poll_reply(artnet_node n) {
  	artnet_reply_t *ar = &n->ar_temp;

  	ESP_LOGI(TAG, "Building buffered ArtPollReply packet");

  	memset(ar, 0x00, sizeof(artnet_reply_t));

  	memcpy(&ar->id, "Art-Net\0", 8);
	ar->opCode = ARTNET_REPLY;
	memcpy(&ar->ip, &n->state.ip_addr.s_addr, 4);
	ar->port = n->state.port;
	ar->verH = FM_VERSION_MAJOR;
	ar->ver = FM_VERSION_MINOR;

	ar->net = n->state.net;
	ar->subnet = n->state.subnet;
	ar->oemH = n->state.oem_hi;
	ar->oem = n->state.oem_lo;
	ar->ubea = 0;

	// status as bit field
	ar->status1 = 0;

	ar->estaman[1] = n->state.esta_hi;  // inversing to account for network byte order
	ar->estaman[0] = n->state.esta_lo;

	// names
	memcpy(&ar->shortname, &n->state.short_name, sizeof(n->state.short_name));
	memcpy(&ar->longname, &n->state.long_name, sizeof(n->state.long_name));

	ar->numbportsH = 0;
	ar->numbports = 1;

	// ports
	for (uint8_t i = 0; i < ARTNET_MAX_PORTS; i++) {
		ar->porttypes[i] = 0;
		ar->goodinput[i] = 0;
		ar->goodoutputA[i] = 0;
		ar->swin[i] = 0;
		ar->swout[i] = 0;
		ar->goodoutputB[i] = 0;
	}

	// hardcoding our only port for now
	ar->porttypes[0] = 0x80; // only 1 output port
	ar->goodoutputA[0] = 0x80;
	ar->swout[0] = 0x00;


	ar->swvideo  = 0;
  	ar->swmacro = 0;
  	ar->swremote = 0;

  	// spares
	ar->sp1 = 0;
	ar->sp2 = 0;
	ar->sp3 = 0;

	ar->style = n->style;

	memcpy(&ar->mac, &n->state.mac_addr, ARTNET_MAC_SIZE);

	// new stuff with ArtNet 3/4
	memcpy(&ar->bindIp, &n->state.ip_addr.s_addr, 4);
	ar->bindIndex = 1;	// no binding = this is the root device
	ar->status2 = 0b00001100;     //TO CHANGE: when more support for stuff (check specs for bit mask)
	if (n->state.dhcp == true) {
		ar->status2 |= 0x02;
	}
	ar->goodoutputB[0] = 0b10000000; //RDM disabled with delta output style
	ar->status3 = 0;

	return 0;
}